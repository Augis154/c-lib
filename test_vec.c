#include "vector.h"
// #include "str.h"
#include <stdio.h>

void foo(vector_t *vec) {
    vector_push(*vec, 2);
}

int main(void) {
    vector_t vec = vector(int);
    vector_t vecv = vector(vector_t);
    vector_t str = vector(char *);

    int a = 1;

    char *s = "Hello";
    char *s1 = "World";

    vector_push(vec, a++);
    vector_push(str, s);
    vector_push(vec, a++);
    vector_push(str, s1);
    vector_push(vec, a++);
    vector_push(str, (char *)"aaaaaaaa");
    vector_push(vec, 9);

    vector_push(vecv, str);

    printf("1: %d\n", vector_get(vec, 1, int));

    vector_set(vec, 1, 5);

    printf("1: %d\n", vector_get(vec, 1, int));

    printf("2: %s\n", vector_get(str, 2, char *));

    vector_insert(vec, 1, 7);

    foo(&vec);

    printf("L: %zu\n", vec.len);

    vector_pop(vec);
    vector_pop(vec);

    printf("L: %zu\n", vec.len);

    printf("S: %zu\n", vec.size);

    printf("F: %d\n", vector_front(vec, int));
    printf("E: %d\n", vector_end(vec, int));

    vector_shrink(vec);

    vector_push(vecv, vec);

    vector_t vv = vector_get(vecv, 1, vector_t);
    printf("1: %d\n", vector_get(vv, 1, int));

    printf("S: %zu\n", vec.size);

    // printf("%d\n", vector_get(vec, 6, int));
    // printf("%s\n", vector_get(vec, 1, char *));

    vector_destroy(vecv);
    vector_destroy(vec);
    vector_destroy(str);

    return 0;
}
