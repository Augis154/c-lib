#include "vector1.h"
// #include "str.h"
#include <stdio.h>

void foo(vector(int) *vec) {
    vector_push(*vec, 2);
}

int main(void) {
    vector(int) vec = vector_new(int);
    vector(char_ptr) str = vector_new(char_ptr);

    // vector(int, vec);

    int a = 1;

    char *s = "Hello";
    char *s1 = "World";

    vector_push(vec, a++);
    vector_push(str, s);
    vector_push(vec, a++);
    vector_push(str, s1);
    vector_push(vec, a++);
    vector_push(str, (char *)"aaaaaaaa");
    vector_push(vec, 9);

    foo((void *)&vec);

    printf("%d\n", vec.at[1]);

    printf("%s\n", str.at[2]);

    printf("%zu\n", vec.len);

    vector_destroy(vec);
}
