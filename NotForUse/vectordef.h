#ifndef VECTOR_H
#define VECTOR_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define char_ptr char *

#define vector(type)                                                           \
  struct vector_##type {                                                       \
    type *at; /* Data */                                                       \
    size_t size;                                                               \
    size_t len;                                                                \
  } vector_t

// #define vector_new(type) { ({type *_data = calloc(1, sizeof(type)); (!_data)
// ? ({ perror("Malloc failed while initializing vector!\n"); abort( ); NULL; })
// : _data; }), 1, 0 };
#define vector_new(type) {calloc(1, sizeof(type)), 1, 0};

// #define vector_init(type) { ({type *_data = calloc(1, sizeof(type)); (!_data)
// ? ({ perror("Malloc failed while initializing vector!\n"); abort( ); NULL; })
// : _data; }) , 1, 0 }

#define vector_push(vector, element)                                           \
  do {                                                                         \
    if (sizeof(*(vector).at) != sizeof((element))) {                           \
      perror("Incompatible type in vector_push!\n");                           \
      abort();                                                                 \
    }                                                                          \
    if ((vector).len + 1 > (vector).size) {                                    \
      (vector).at =                                                            \
          realloc((vector).at, (vector).size * 2 * sizeof((element)));         \
      if (!(vector).at) {                                                      \
        perror("Expanding vector failed!\n");                                  \
        abort();                                                               \
      }                                                                        \
      (vector).size *= 2;                                                      \
    }                                                                          \
    __typeof__((element)) tmp = (element);                                     \
    (vector).len++;                                                            \
    (vector).at[(vector).len - 1] = tmp;                                       \
  } while (0)

#define vector_pop(vector) (vector).len--;
#define vector_front(vector) (vector).at[0];
#define vector_back(vector) (vector).at[(vector).len];
#define vector_erase(vector) (vector).len = 0;

#define vector_remove(vector, index)                                           \
  memcpy((vector)->at + index - 1, (vector)->at + index,                       \
         (((vector)->length--) - pos + 1) * sizeof(*(vector)->at));

#define vector_destroy(vector)                                                 \
  do {                                                                         \
    (vector).len = 0;                                                          \
    (vector).size = 0;                                                         \
    free((vector).at);                                                         \
  } while (0);

// #define vector_len(vector) (vector)->len;

#endif
