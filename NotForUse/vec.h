#ifndef VECTOR_H
#define VECTOR_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct vector_t;

// void _append(struct _vector *vector, void* element);
// void _prepend(struct _vector *vector, void *element);
// void _remove(struct _vector *vector, int pos);
// void _destroy(struct _vector *vector);
// void _reset(struct _vector *vector);

// Declare a vector of type `TYPE`
// #define vector(TYPE) \
// struct vector_t {\
//         TYPE *data = malloc(1 * sizeof(TYPE));                                    \
//         size_t size = 1;                                   \
//         size_t length = 0;                                 \
// }

// #define VECTOR_OF(TYPE) struct { \
//     TYPE *data; \
//     size_t size; \
//     size_t capacity; \
// }

#define vector(TYPE) ({\
struct vector_t {\
        TYPE *data;                                    \
        size_t size;                                   \
        size_t length;                                 \
} _vector;\
_vector.data = malloc(1 * sizeof(*_vector.data)); \
    if (!(_vector).data) {                            \
        perror("Malloc failed!\n");               \
        abort();                                  \
    }                                             \
    _vector.size = 1;                               \
    _vector.length = 0;\
_vector;\
})


/* Initialize `VEC` */
#define vec_init(VEC)                             \
    (VEC).data = malloc(1 * sizeof(*(VEC).data)); \
    if (!(VEC).data) {                            \
        perror("Malloc failed!\n");               \
        abort();                                  \
    }                                             \
    (VEC).size = 1;                               \
    (VEC).length = 0;

/* Get the amount of elements in `VEC`. */
#define VECTOR_SIZE(VEC) (VEC).size

/* Get the amount of elements that are allocated for `VEC`. */
#define VECTOR_CAPACITY(VEC) (VEC).length

/* Test if `VEC` is empty. */
#define VECTOR_EMPTY(VEC) ((VEC).size == 0)

/* Push `VAL` at the back of the vector. This function will reallocate the buffer if
   necessary. */
#define VECTOR_PUSH_BACK(VEC, VAL)                                  \
    do {                                                            \
        if ((VEC).size + 1 > (VEC).capacity) {                      \
            size_t n = (VEC).capacity * 2;                          \
            void* p = realloc((VEC).data, n * sizeof(*(VEC).data)); \
            if (!p) {                                               \
                fputs("realloc failed!\n", stderr);                 \
                abort();                                            \
            }                                                       \
            (VEC).data = p;                                         \
            (VEC).capacity = n;                                     \
        }                                                           \
        (VEC).data[VECTOR_SIZE(VEC)] = (VAL);                       \
        (VEC).size += 1;                                            \
    } while (0)

   /* Get the value of `VEC` at `INDEX`. */
#define VECTOR_AT(VEC, INDEX) (VEC).data[INDEX]

/* Get the value at the front of `VEC`. */
#define VECTOR_FRONT(VEC) (VEC).data[0]

/* Get the value at the back of `VEC`. */
#define VECTOR_BACK(VEC) (VEC).data[VECTOR_SIZE(VEC) - 1]

#define VECTOR_FREE(VEC)    \
    do {                    \
        (VEC).size = 0;     \
        (VEC).capacity = 0; \
        free((VEC).data);   \
    } while (0)

#endif
