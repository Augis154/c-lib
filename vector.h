#ifndef _VECTOR_H
#define _VECTOR_H

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// #include <stdint.h>

typedef struct _vector_t vector_t;
// typedef struct _vector_functions_t vector_functions_t;

typedef char *_vector_data_t;

struct _vector_t {
  _vector_data_t data;
  size_t data_size;
  size_t size;
  size_t len;
};

vector_t _vector_init(size_t data_size) {
  struct _vector_t _vector;
  _vector.data = (_vector_data_t)calloc(1, data_size);
  // _vector.data = malloc(1 * data_size);
  if (!_vector.data) {
    perror("Malloc failed while initializing vector!\n");
    abort();
  }
  _vector.data_size = data_size;
  _vector.size = 1;
  _vector.len = 0;

  return _vector;
}

void _vector_push(struct _vector_t *vector, const _vector_data_t element,
                  const size_t element_size) {
  if (element_size != vector->data_size) {
    perror("Incompatible element type in vector_push!\n");
    // abort( );
    return;
  }
  if (vector->len + 1 > vector->size) {
    vector->data = (_vector_data_t)realloc(vector->data, vector->data_size *
                                                             vector->size * 2);
    if (!vector->data) {
      perror("Expanding vector failed in vector_push!\n");
      abort();
    }
    vector->size *= 2;
  }
  memmove(vector->data + vector->data_size * vector->len, element,
          vector->data_size);
  vector->len++;
}

_vector_data_t _vector_get(struct _vector_t *vector, const size_t index,
                           const size_t element_size) {
  if (element_size != vector->data_size) {
    perror("Incompatible type in vector_get!\n");
    // abort( );
    return NULL;
  }
  if (index > vector->len) {
    perror("Accessing data out of bounds in vector_get!\n");
    return NULL;
  }
  return (vector->data + vector->data_size * (index));
}

void _vector_set(struct _vector_t *vector, const size_t index,
                 const _vector_data_t element, const size_t element_size) {
  if (element_size != vector->data_size) {
    perror("Incompatible element type in vector_set!\n");
    // abort( );
    return;
  }
  if (index > vector->len) {
    perror("Accessing data out of bounds in vector_get!\n");
    return;
  }
  memmove(vector->data + vector->data_size * index, element, vector->data_size);
}

void _vector_insert(struct _vector_t *vector, const size_t index,
                    const _vector_data_t element, const size_t element_size) {
  if (element_size != vector->data_size) {
    perror("Incompatible element type in vector_insert!\n");
    // abort( );
    return;
  }
  if (index > vector->len) {
    return;
  }

  if (vector->len + 1 > vector->size) {
    vector->data = (_vector_data_t)realloc(vector->data, vector->data_size *
                                                             vector->size * 2);
    if (!vector->data) {
      perror("Expanding vector failed in vector_insert!\n");
      abort();
    }
    vector->size *= 2;
  }
  size_t offset = vector->data_size * index;
  memmove(vector->data + offset + vector->data_size, vector->data + offset,
          vector->data_size * vector->len - offset);
  memmove(vector->data + offset, element, vector->data_size);

  vector->len++;
}

void _vector_remove(struct _vector_t *vector, const size_t index) {
  if (index > vector->len) {
    return;
  }
  memcpy(vector->data + index - 1, vector->data + index,
         vector->data_size * (vector->size - index + 1));
  vector->len--;
}

void _vector_destroy(struct _vector_t *vector) {
  vector->len = 0;
  vector->size = 0;
  free(vector->data);
}

void _vector_shrink(struct _vector_t *vector) {
  _vector_data_t data = (_vector_data_t)malloc(vector->data_size * vector->len);
  if (!data) {
    perror("Shrinking vector failed!\n");
    // abort( );
    return;
  }
  memmove(data, vector->data, vector->data_size * vector->len);
  free(vector->data);
  vector->data = data;
  vector->size = vector->len;
}

#define vector(type) _vector_init(sizeof(type));

#define vector_push(vector, element)                                           \
  {                                                                            \
    __typeof__(element) x = (element);                                         \
    _vector_push(&(vector), (_vector_data_t)(&x), sizeof(x));                  \
  }

#define vector_pop(vector) (vector).len--

#define vector_get(vector, index, type)                                        \
  *((type *)_vector_get(&(vector), (index), sizeof(type)))

#define vector_set(vector, index, element)                                     \
  {                                                                            \
    __typeof__(element) x = (element);                                         \
    _vector_set(&(vector), index, (_vector_data_t)(&x), sizeof(x));            \
  }

#define vector_insert(vector, index, element)                                  \
  {                                                                            \
    __typeof__(element) x = (element);                                         \
    _vector_insert(&(vector), index, (_vector_data_t)(&x), sizeof(x));         \
  }

#define vector_front(vector, type) vector_get(vector, 0, type)
#define vector_end(vector, type) vector_get(vector, (vector).len, type)

#define vector_remove(vector, index) _vector_remove(&vector, index)
#define vector_destroy(vector) _vector_destroy(&vector)
#define vector_shrink(vector) _vector_shrink(&vector)

#endif // _VECTOR_H
