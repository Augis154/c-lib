# C libraries

Collection of single-file c libraries

## Libraries

### str.h
Library for manipulating c strings

### vector.h
Simple generic vector library

### kbhit.h
Library for detecting whether there is a keyboard input in terminal
